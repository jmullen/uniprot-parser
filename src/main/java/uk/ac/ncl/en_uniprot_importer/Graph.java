/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.en_uniprot_importer;

/**
 *
 * @author joemullen
 */
import com.entanglementgraph.graph.commands.EdgeUpdate;
import com.entanglementgraph.graph.commands.GraphOperation;
import com.entanglementgraph.graph.commands.NodeUpdate;
import com.entanglementgraph.graph.couchdb.CouchGraphConnection;
import com.entanglementgraph.util.TxnUtils;
import org.openide.util.Exceptions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author Joseph Mullen http://intbio.ncl.ac.uk/?people=joe-mullen & Michael
 * Bell
 *
 */
public class Graph {
    private static final int DEFAULT_SUBMIT_SIZE = 50000;

    private LinkedList<GraphOperation> graphOps;
    private Map<Class, String> mappings;
    private CouchGraphConnection graphConn;
    private int submitSize;

    public Graph(CouchGraphConnection graphConn) {
        this(graphConn, DEFAULT_SUBMIT_SIZE);
    }

  public Graph(CouchGraphConnection graphConn, int subSize) {
        
        this.graphConn = graphConn;
        this.submitSize = subSize;
        this.graphOps = new LinkedList<GraphOperation>();
        this.mappings = new HashMap<Class, String>();
        
    }

    
    public void addNodeUpdate(NodeUpdate latest) {
        
        try {
            graphOps.add(latest);
            
            if (graphOps.size() == submitSize) {
                submitTransaction(graphOps);
                graphOps.clear();
            }
        } catch (Exception ex) {
            
            Exceptions.printStackTrace(ex);
        }  
        
    }
    
     public void addEdgeUpdate(EdgeUpdate latest) {
        
        try {
            graphOps.add(latest);
            
            if (graphOps.size() == submitSize) {
                submitTransaction(graphOps);
                graphOps.clear();
            }
        } catch (Exception ex) {         
            Exceptions.printStackTrace(ex);
        }  
        
    }
    
    public void submitTransaction(LinkedList<GraphOperation> operations) {
        try {
            TxnUtils.submitAsTxn(graphConn, operations);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void finalTransaction() {
        
        submitTransaction(graphOps);
        graphOps.clear();
          
    }
}
