/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package uk.ac.ncl.en_uniprot_importer;

import com.entanglementgraph.graph.EntityKeys;
import com.entanglementgraph.graph.Node;
import com.entanglementgraph.graph.couchdb.CouchGraphConnection;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.microbasecloud.genomepool.data.GeneProduct;
import com.scalesinformatics.util.UidGenerator;
import uk.org.microbase.graph.files.MBFile;
import uk.org.microbase.graph.messaging.Message;
import uk.org.microbase.graph.messaging.ResponderType;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.responder.AbstractMessageProcessor;
import uk.org.microbase.responder.ProcessingException;
import uk.org.microbase.responder.ResponderInitialisationException;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * A responder that executes the a Uniprot parser. This parser requires a standard Uniprot flat file as input.
 * The content is parsed and stored into an Entanglement graph.
 *
 *
 * Input message properties:
 * <ul>
 *   <li>input_db_bucket - specifies the 'bucket' containing the input Uniprot file</li>
 *   <li>input_db_path - specifies the parent path to the input Uniprot file (excluding the filename)</li>
 *   <li>input_db_filename - specifies the filename of the input Uniprot file</li>
 * </ul>
 *
 * Environment variables:
 * <ul>
 *   <li>max_proteins_to_parse - specifies the maximum number of proteins to parse from a given file. This option is
 *   primarily useful for testing purposes. By default, all proteins are parsed. </li>
 *   <li>result_graph_name - specifies the connection name of an Entanglement graph to use for storing result data</li>
 * </ul>
 *
 * The environment properties listed above may also be specified in message content, if you wish.
 *
 * @author Keith Flanagan
 */
public class UniprotResponder extends AbstractMessageProcessor {
  private static final int DEFAULT_MAX_PROTS_TO_PARSE = Integer.MAX_VALUE;

  private String responderName;
  private Node<ResponderType> rt;

  private CouchGraphConnection resultGraph;
  private int maxProteinsToParse;
  private Node<MBFile> inputFileRemote;

  @Override
  public void initialise() throws ResponderInitialisationException {
    /*
     * This gets called by Microbase first. You can use this method to perform basic configuration - perhaps
     * inspect the responder configuration, maybe set up a database connection, etc.
     */
    rt = responderInfo.getResponderType();
    responderName = rt.getKeys().getSingletonUid();

    ircLogger.println("%s Initialising instance of responder: %s",
        formatter.bullet().toString(),
        formatter.formatResponderId(rt).toString()
    );
  }

  @Override
  public void preRun(Node<Message> incomingMsgNode) throws ProcessingException {
    /*
     * This gets run after initialise(). Here, you can configure anything else that needs configuring, maybe based
     * on some message property.
     */
    ircLogger.println("%s Pre-run: %s",
        formatter.bullet().toString(),
        formatter.formatResponderId(rt).toString()
    );

    /*
     * Parse incoming message
     */
    inputFileRemote = MBFile.create(
        locateProperty(incomingMsgNode, "input_db_bucket"),
        locateProperty(incomingMsgNode, "input_db_path"),
        locateProperty(incomingMsgNode, "input_db_filename"));


    /*
     * Read configuration properties
     */
    String maxProteinsToParseStr = locateProperty(incomingMsgNode, "max_proteins_to_parse");
    maxProteinsToParse = maxProteinsToParseStr != null
        ? Integer.parseInt(maxProteinsToParseStr) : DEFAULT_MAX_PROTS_TO_PARSE;

    String resultGraphName = locateProperty(incomingMsgNode, "result_graph_name");
    try {
      resultGraph = runtime.getEntanglementRuntime().createGraphConnectionFor(resultGraphName);
    } catch (Exception e) {
      throw new ProcessingException("Failed to obtain graph connection for name: "+resultGraphName, e);
    }

  }

  /**
   * Attempts to locate the value for the specified property. If the property exists within the incoming message
   * content, then that value is returned, otherwise the responder's configuration environment is checked.
   * @param propName the name of the property whose value should be returned
   * @return the value of <code>propName</code> or NULL if the property could not be found.
   */
  private String locateProperty(Node<Message> incomingMsgNode, String propName) {
    Map<String, String> msgContent = incomingMsgNode.getContent().getContent();
    Map<String, String> envContent = getResponderConfiguration().getEnvironment();
    return msgContent.containsKey(propName) ? msgContent.get(propName) : envContent.get(propName);
  }

  @Override
  public void cleanupPreviousResults(Node<Message> incomingMsgNode) throws ProcessingException {
    /*
     * Here, you get a chance to clean up any mess made by a previous (failed) attempt at processing this message.
     * Tidy up files, database records, etc, as appropriate.
     */
    ircLogger.println("%s Cleanup: %s",
        formatter.bullet().toString(),
        formatter.formatResponderId(rt).toString()
    );
  }

  @Override
  public Set<MicrobaseNotification.Publication> processMessage(Node<Message> incomingMsgNode) throws ProcessingException {
    /*
     * Now we're ready to actually process the message. Do any required processing here and (optionally) return
     * a set of Microbase messages.
     *
     * Here, we perform the following:
     *
     * 1) Download required input file(s)
     * 2) Execute a command line
     * 3) Upload output result file(s)
     */

    ircLogger.println("%s Processing: %s using %s local cores",
        formatter.bullet().toString(),
        formatter.formatResponderId(rt).toString(),
        formatter.format(process.getAssignedCpus()));

    ircLogger.println("%s STDIN: %s",
        formatter.bullet(2).toString(),
        formatter.formatTopicId(responderInfo.getStdIn()).toString());
    ircLogger.println("%s STDOUT: %s",
        formatter.bullet(2).toString(),
        formatter.formatTopicId(responderInfo.getStdOut()).toString());
    ircLogger.println("%s STDERR: %s",
        formatter.bullet(2).toString(),
        formatter.formatTopicId(responderInfo.getStdErr()).toString());

    // Acquire uniprot file for parsing
    File inputFile = downloadFile(inputFileRemote);



    // Delegate onto parser for the actual work

    ircLogger.println("%s Executing parser",
        formatter.bullet(2).toString());
    Parse parser = new Parse(inputFile.getAbsolutePath(), maxProteinsToParse, resultGraph) ;
    try {
      parser.execute();
    } catch (IOException e) {
      throw new ProcessingException("Failed to execute parser", e);
    }

    ircLogger.println("%s Complete!",
        formatter.bullet(2).toString());

    /*
     * Here's how to construct a message to be published, describing the results that we successfully produced.
     * First, we create a Message object, with some content. This content partly consists of some human-readable
     * text, and also contains a property
     *
     * Next, we wrap it in a publication.
     * Finally, we return the message back to Microbase for publication.
     */
    Node<Message> successMsgNode = new Node<>(Message.getTypeName(), UidGenerator.generateUid(), new Message());
    Message successMsg = successMsgNode.getContent();
    successMsg.getContent().put("uniprot_db_bucket", inputFileRemote.getContent().getBucket());
    successMsg.getContent().put("uniprot_db_path", inputFileRemote.getContent().getPath());
    successMsg.getContent().put("uniprot_db_filename", inputFileRemote.getContent().getName());

    successMsg.getContent().put("uniprot_result_graph", resultGraph.getGraphName());

    successMsg.getContent().put("num_accessions", String.valueOf(parser.getAccessionCount()));
    successMsg.getContent().put("num_proteins", String.valueOf(parser.getProteinSeqCount()));
    successMsg.getContent().put("num_go", String.valueOf(parser.getGoCount()));
    successMsg.getContent().put("num_ko", String.valueOf(parser.getKoCount()));

    try {
      ObjectMapper om = new ObjectMapper();
      successMsg.getContent().put("protein_ids", om.writeValueAsString(generateProteinIdList(parser.getAllProteins())));
    } catch (JsonProcessingException e) {
      throw new ProcessingException("Failed to serialize one or more items", e);
    }

    MicrobaseNotification.Publication successPub = new MicrobaseNotification.Publication();
    successPub.setParentMsg(incomingMsgNode);
    successPub.setPublisher(responderInfo.getResponderType());
    successPub.setTopic(responderInfo.getStdOut());
    successPub.setMessage(successMsgNode);

    Set<MicrobaseNotification.Publication> pubs = new HashSet<>();
    pubs.add(successPub);

    ircLogger.println("%s Completed. Will publish %s messages on exit.",
        formatter.bullet().toString(),
        formatter.format(pubs.size()).toString());

    return pubs;
  }

  private File downloadFile(Node<MBFile> source) throws ProcessingException {
    try {
      File destination = new File(getWorkingDir(), source.getContent().getName());
      fs.downloadFile(source, destination);
      return destination;
    } catch (Exception e) {
      throw new ProcessingException("Failed to download file from remote location: "+source.toString(), e);
    }
  }

  private List<EntityKeys> generateProteinIdList(Collection<Node<GeneProduct>> products) {
    List<EntityKeys> ids = new LinkedList<>();
    for (Node<GeneProduct> prod : products) {
      ids.add(prod.getKeys());
    }
    return ids;
  }

  @Override
  public void notifyProgress(float percentComplete) throws ProcessingException {
    //To change body of implemented methods use File | Settings | File Templates.
  }
}
