package uk.ac.ncl.en_uniprot_importer;

/*
 * Copyright 2013 Joseph Mullen
 *
 */
import com.entanglementgraph.graph.Edge;
import com.entanglementgraph.graph.EntityKeys;
import com.entanglementgraph.graph.Node;
import com.entanglementgraph.graph.commands.EdgeUpdate;
import com.entanglementgraph.graph.commands.NodeUpdate;
import com.entanglementgraph.graph.couchdb.CouchGraphConnection;
import com.microbasecloud.genomepool.data.GeneProduct;
import com.scalesinformatics.util.UidGenerator;
import uk.ac.ncl.en_uniprot_importer.RelationTypes.has_GO_term;
import uk.ac.ncl.en_uniprot_importer.RelationTypes.has_KO_term;

import java.io.*;
import java.util.*;

/**
 *
 * @author Joseph Mullen http://intbio.ncl.ac.uk/?people=joe-mullen
 *
 */
public class Parse {

    private final String uniprotDB;
    private final int numberOfProteins;
    private final Graph uniGraph;
    private final Graph goGraph;
    private final Graph relsGOGraph;
    private final Graph relsKOGraph;

    private int accessionCount = 0;
    private int proteinSeqCount = 0;
    private int goCount = 0;
    private int koCount = 0;

    private final List<Node<GeneProduct>> allProteins = new LinkedList<>();

    public Parse(String dbloc, int proteins, CouchGraphConnection resultConn) {

        this.numberOfProteins = proteins;
        this.uniprotDB = dbloc;
        this.uniGraph = new Graph(resultConn);
        this.goGraph = new Graph(resultConn);
        this.relsGOGraph = new Graph(resultConn);
        this.relsKOGraph = new Graph(resultConn);
    }


    public Parse(String dbloc, int proteins, CouchGraphConnection uniConn, CouchGraphConnection goConn,
               CouchGraphConnection relsGoConn, CouchGraphConnection relsKoConn) {

      this.numberOfProteins = proteins;
      this.uniprotDB = dbloc;
      this.uniGraph = new Graph(uniConn);
      this.goGraph = new Graph(goConn);
      this.relsGOGraph = new Graph(relsGoConn);
      this.relsKOGraph = new Graph(relsKoConn);
    }

    public Parse(String dbloc, int proteins, CouchGraphConnection uniConn, CouchGraphConnection goConn,
                 CouchGraphConnection relsGoConn, CouchGraphConnection relsKoConn, int subSize)  {

        this.numberOfProteins = proteins;
        this.uniprotDB = dbloc;
        this.uniGraph = new Graph(uniConn, subSize);
        this.goGraph = new Graph(goConn, subSize);
        this.relsGOGraph = new Graph(relsGoConn, subSize);
        this.relsKOGraph = new Graph(relsKoConn, subSize);
    }

    public void execute() throws IOException {
        InputStream stream = null;
        InputStreamReader reader = null;
        BufferedReader buffered = null;
        String seq = "";
        boolean getSeq = false;
        String acc = "";
        int count = 0;
        Set<String> goNodes = new HashSet<String>();
        Set<String> koNodes = new HashSet<String>();

        Set<String> geneIds = new HashSet<>(); // Any GeneID
        Set<String> refSeqProtIds = new HashSet<>(); // Any NP_* ids, eg: NP_115290.1  (see: http://en.wikipedia.org/wiki/RefSeq)

        try {
            // open input stream test.txt for reading purpose.
            stream = new FileInputStream(uniprotDB);
            // create new input stream reader
            reader = new InputStreamReader(stream);
            // create new buffered reader
            buffered = new BufferedReader(reader);
            String line;
            // reads to the end of the stream 
            while ((line = buffered.readLine()) != null) {

                //this is where the parsing will go

                if (line.startsWith("AC")) {
                    acc = line.substring(5, 11);
                    accessionCount++;
                }

                if (line.startsWith("//")) {
                    //create protein node
                    Node<GeneProduct> from = createEntProt(refSeqProtIds, geneIds, acc, seq.trim());
                    allProteins.add(from);

                    //create hanging relations between the protein and the GO terms
                    for (String GO : goNodes) {
                        createEntHasGO(from.getKeys(), new EntityKeys("GO", GO));

                    }

                    //create hanging edges between the protein and all the KO terms
                    for (String KO : koNodes) {
                        createEntHasKO(from.getKeys(), new EntityKeys("KO", KO) );

                    }


                    proteinSeqCount++;
                    count++;
                    if (count == numberOfProteins) {
                        break;
                    }

                    //reset data structures
                    goNodes.clear();
                    koNodes.clear();
                    refSeqProtIds.clear();
                    geneIds.clear();
                    getSeq = false;
                    seq = "";
                    acc = "";

                }

                if (line.startsWith("DR")) {

                    // Parse GeneIDs and RefSeq IDs
                    // We use '-1' here to filter the end-of-line dot
                    StringTokenizer idFinder = new StringTokenizer(line.substring(5, line.length()-1), "; ");
                    switch (idFinder.nextToken()) {
                      case "RefSeq" :
                        for (String next = idFinder.nextToken(); idFinder.hasMoreTokens(); next = idFinder.nextToken()) {
                          // We ONLY want NP_* IDs, since these refer to the protein
                          if (next.startsWith("NP_")) {
                            refSeqProtIds.add(next);
                          }
                        }
                        break;
                      case "GeneID" :
                        String firstGeneId = idFinder.nextToken();
                        if (!firstGeneId.contains("-")) {
                          geneIds.add(firstGeneId);
                        }
                        break;
                    }




                    // Parse GO/KO terms
                    String line2 = line.substring(5, line.length());
                    StringTokenizer st = new StringTokenizer(line2, ";");
                    while (st.hasMoreTokens()) {
                        String token = st.nextToken().trim();

                        if (token.length() == 10 && token.startsWith("GO")) {
                            goNodes.add(token);
                            goCount++;
                        }

                        if (token.equals("KO")) {
                            koNodes.add(st.nextToken().trim());
                            koCount++;
                        }
                    }


                }

                if (getSeq == true) {
                    seq = seq.trim() + " " + line.trim();
                }

                if (line.startsWith("SQ")) {
                    getSeq = true;
                    // seq = line;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            // releases resources associated with the streams
            if (stream != null) {
                stream.close();
            }
            if (reader != null) {
                reader.close();
            }
            if (buffered != null) {
                buffered.close();
            }
        }

        uniGraph.finalTransaction();
        goGraph.finalTransaction();
        relsGOGraph.finalTransaction();
        relsKOGraph.finalTransaction();

    }

    @SuppressWarnings("unchecked")
    public Node<GeneProduct> createEntProt(Set<String> refSeqProtIds, Set<String> geneIds, String acc, String seq) {
        Node<GeneProduct> uniEntry = new Node<>(GeneProduct.getTypeName(), new GeneProduct());
        String uid = "UniProt:"+acc;
        uniEntry.getKeys().addUid(uid);
        uniEntry.getContent().setSequence(seq);

//        UniprotProtein p = new UniprotProtein(acc, seq);
//        Node<UniprotProtein> uniEntry = new Node<UniprotProtein>(new EntityKeys(UniprotProtein.class.getSimpleName(), acc), p);


        for (String id : refSeqProtIds) {
          uniEntry.getKeys().addUid("RefSeq:"+id); // Ensure naming remains consistent with other parsers
        }

        for (String id : geneIds) {
          uniEntry.getKeys().addUid("GeneID:"+id); // Ensure naming remains consistent with other parsers
        }

        uniGraph.addNodeUpdate(new NodeUpdate(uniEntry));
        return uniEntry;
    }

//    public Node<GOTerm> createEntGO(String GOterm) {
//        GOTerm go = new GOTerm();
//        Node<GOTerm> goterm = new Node<GOTerm>(new EntityKeys(GOTerm.class.getName(), GOterm), go);
//        goGraph.addNodeUpdate(new NodeUpdate(goterm));
//        return goterm;
//    }
//                 
    public void createEntHasGO(EntityKeys from, EntityKeys to) {
        Edge edge = new Edge(new EntityKeys(has_GO_term.class.getSimpleName(), UidGenerator.generateUid()));
        edge.setFrom(from);
        edge.setTo(to);
        relsGOGraph.addEdgeUpdate(new EdgeUpdate(edge));
    }

    public void createEntHasKO(EntityKeys from, EntityKeys to) {
        Edge edge = new Edge(new EntityKeys(has_KO_term.class.getSimpleName(), UidGenerator.generateUid()));
        edge.setFrom(from);
        edge.setTo(to);
        relsKOGraph.addEdgeUpdate(new EdgeUpdate(edge));
    }

  public int getKoCount() {
    return koCount;
  }

  public int getGoCount() {
    return goCount;
  }

  public int getProteinSeqCount() {
    return proteinSeqCount;
  }

  public int getAccessionCount() {
    return accessionCount;
  }

  public List<Node<GeneProduct>> getAllProteins() {
    return allProteins;
  }
}
