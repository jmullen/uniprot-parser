/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.en_uniprot_importer;

import com.entanglementgraph.graph.GraphConnectionFactoryException;
import com.entanglementgraph.graph.couchdb.CouchGraphConnection;
import com.entanglementgraph.graph.couchdb.CouchGraphConnectionFactory;
import org.openide.util.Exceptions;

import java.io.IOException;

/**
 *
 * @author joemullen
 */
public class Main {

    public static void main(String[] args) {
        int length = args.length;
        if (length < 9) {
            System.out.println("You need to enter 9 arguments:");
            System.out.println("\t" + "[0] Uniprot .dat location ");
            System.out.println("\t" + "[1] Number of Proteins to be parsed");
            System.out.println("\t" + "[2] Clustername (name of the CouchDB cluster)");
            System.out.println("\t" + "[3] Cluster (URL to use as CouchDB server)");
            System.out.println("\t" + "[4] Database name ");
            System.out.println("\t" + "[5] GraphName ");
            System.out.println("\t" + "[6] Username ");
            System.out.println("\t" + "[7] Password ");
            System.out.println("\t" + "[8] Transaction submit size ");            
        }
        
        else{
        
            String dbloc = args[0];
            int numProteins = Integer.parseInt(args[1]);
            String clusterName = args[2];
            String cluster = args[3];
            String dbname = args[4];
            String graphname = args[5];
            String username = args[6];
            String password = args[7];
            int subSize = Integer.parseInt(args[8]);
            
            try {
                // Define the database cluster (at the moment, just one database server URL)
                CouchGraphConnectionFactory.registerNamedCluster(clusterName, cluster);

                // Create a connection factory
                CouchGraphConnectionFactory connFact = new CouchGraphConnectionFactory(clusterName, dbname, username, password);
                CouchGraphConnection uniGraph = connFact.connect(graphname);
                CouchGraphConnection goGraph = connFact.connect("GO");
                CouchGraphConnection relsGoGraph = connFact.connect("GO-Uniprot");
                CouchGraphConnection relsKoGraph = connFact.connect("KO-Uniprot");

                Parse p = new Parse( dbloc, numProteins, uniGraph, goGraph, relsGoGraph, relsKoGraph, subSize);
                p.execute();
//              Parse p = new Parse("/Users/joemullen/Desktop/Uniprot_Entanglement_Parser/uniprot_sprot.dat", 100, "local", "http://127.0.0.1:5984", "Uniprot", "root", "Joe", "secret", 100);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
           } catch (GraphConnectionFactoryException ex) {
                Exceptions.printStackTrace(ex);
            }

        }     

    }
}
